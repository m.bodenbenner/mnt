#include <NTPClient.h>

#include <PubSubClient.h>

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <time.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define NTP_OFFSET 0
#define NTP_INTERVAL 60 * 1000
#define NTP_ADDRESS "europe.pool.ntp.org"
#define FIRST_ONE_WIRE_BUS 2
#define SECOND_ONE_WIRE_BUS 0

OneWire firstOneWire(FIRST_ONE_WIRE_BUS);
OneWire secondOneWire(SECOND_ONE_WIRE_BUS);
DallasTemperature FirstDS18B20(&firstOneWire);
DallasTemperature SecondDS18B20(&secondOneWire);

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, NTP_ADDRESS, NTP_OFFSET, NTP_INTERVAL);

int pin = 2;
int outputpin = 0;

int status = WL_IDLE_STATUS;
byte mac[6];

const char *SSID = "";
const char *PSK = "";

const char *MQTT_BROKER = "";
const char *USER = "";
const char *PWD = "";

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[100];
int value = 0;
float temperature;

void setup()
{
    Serial.begin(9600);
    setup_wifi();
    client.setServer(MQTT_BROKER, 1883);
    timeClient.begin();
    FirstDS18B20.begin();
    SecondDS18B20.begin();
}

void setup_wifi()
{
    delay(10);
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(SSID);

    WiFi.begin(SSID, PSK);

    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());

    digitalWrite(pin, HIGH);
    Serial.print("output is high\n");
}

void reconnect()
{
    while (!client.connected())
    {
        Serial.print("Reconnecting...");
        if (!client.connect("ESP8266Client", USER, PWD))
        {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" retrying in 5 seconds");
            delay(5000);
        }
    }
}

time_t Timestamp()
{
    timeClient.update();
    time_t timestamp = timeClient.getEpochTime();
    return timestamp;
}

String RFC3339Timestamp(time_t timestamp) {
    struct tm ts;
    char rfc3339[100];
    ts = *localtime(&timestamp);
    strftime(rfc3339, sizeof(rfc3339), "%Y-%m-%dT%H:%M:%SZ", &ts);
    return rfc3339;
}

void loop()
{

    if (!client.connected())
    {
        reconnect();
    }
    client.loop();

    time_t timestamp;
    FirstDS18B20.requestTemperatures();
    temperature = FirstDS18B20.getTempCByIndex(0);
    timestamp = Timestamp();

    String message = "{\"value\": " + String(temperature) + ", \"timestamp\": \"" + RFC3339Timestamp(timestamp) + "\"}";
    message.toCharArray(msg, message.length() + 1);
    client.publish("mnt/OBJ-PhD-Hat/VAR-LeftTemperature", msg);
    message = "temperature,sensor=left temperature="+String(temperature)+" "+String(timestamp*1e9).substring(0, 19);
    message.toCharArray(msg, message.length() + 1);
    client.publish("mnt/influx/OBJ-PhD-Hat/VAR-LeftTemperature", msg);
 
    SecondDS18B20.requestTemperatures();
    temperature = SecondDS18B20.getTempCByIndex(0);
    timestamp = Timestamp();

    message = "{\"value\": " + String(temperature) + ", \"timestamp\": \"" + RFC3339Timestamp(timestamp) + "\"}";
    message.toCharArray(msg, message.length() + 1);
    client.publish("mnt/OBJ-PhD-Hat/VAR-RightTemperature", msg);
    message = "temperature,sensor=right temperature="+String(temperature)+" "+String(timestamp*1e9).substring(0, 19);
    message.toCharArray(msg, message.length() + 1);
    client.publish("mnt/influx/OBJ-PhD-Hat/VAR-RightTemperature", msg);

    delay(1000);
}
