import {Card, Container, Grid} from '@material-ui/core'
import React, {useEffect} from 'react'
import {makeStyles} from '@material-ui/core/styles'
import {useDispatch} from 'react-redux'
import {mqttConnect, mqttSubscribe} from '../actions/mqtt'
import NewsFeed from '../container/layout/newsfeed'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: '#eceded',
    display: 'flex',
    flexDirection: 'column'
  },
  content: {},
  ruuviCard: {
    width: '100%',
    height: '20rem',
    marginTop: '2rem'
  },
  hatCard: {
    width: '100%%',
    height: '25rem',
    marginTop: '2rem'
  }
}))

function Home() {
  const classes = useStyles()
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(mqttConnect('', '', ''))
    dispatch(mqttSubscribe('mnt/OBJ-PhD-Hat/VAR-LeftTemperature'))
    dispatch(mqttSubscribe('mnt/OBJ-PhD-Hat/VAR-RightTemperature'))
  }, [dispatch])

  return (
    <div>
      <Container>
        <Grid container className={classes.root}>
          <Grid item xs={12}>
            <Card className={classes.ruuviCard}>
              <iframe
                src="https://iot.wzl-mq.rwth-aachen.de/public/dashboards/d-solo/xH9ILbInk/new-dashboard?orgId=1&from=1548078479000&to=1624381685000&panelId=2"
                width="100%" height="100%" frameBorder="0"/>
            </Card>
          </Grid>
        </Grid>
      </Container>
      <NewsFeed/>
      <Container>
        <Grid container>
          <Grid item xs={6} style={{paddingRight: '1rem'}}>
            <Card className={classes.hatCard}>
              <iframe
                src="https://asse-sind-klasse.de/mnt/dashboards/d-solo/wtlK_xS7k/mnt?orgId=1&&theme=light&refresh=5s&panelId=2"
                width="100%" height="100%" frameBorder="0"/>
            </Card>
          </Grid>
          <Grid item xs={6} style={{paddingLeft: '1rem'}}>
            <Card className={classes.hatCard}>
              <iframe
                src="https://asse-sind-klasse.de/mnt/dashboards/d-solo/wtlK_xS7k/mnt?orgId=1&theme=light&refresh=5s&panelId=3"
                width="100%" height="100%" frameBorder="0"/>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </div>
  )
}

export default Home
