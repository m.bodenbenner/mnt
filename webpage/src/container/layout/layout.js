import './Layout.css'
import NewsFeed from './newsfeed'
import Header from './header'
import CssBaseline from '@material-ui/core/CssBaseline'
import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import Footer from './footer'
import Home from '../../views/home'

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: "#eceded",
        display: "flex",
        flexDirection: "column",
    },
    content: {
      display: "flex",
      flexDirection: "column",
    },
}));

function Layout() {
    const classes = useStyles();

    return (
        <div className={`App ${classes.root}`}>
            <CssBaseline/>
            <Header/>
            <Home/>
            <Footer/>
        </div>
    );
}

export default Layout;
