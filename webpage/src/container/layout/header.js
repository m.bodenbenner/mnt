import './Layout.css'
import AppBar from '@material-ui/core/AppBar'
import Typography from '@material-ui/core/Typography'
import Hidden from '@material-ui/core/Hidden'
import {Grid} from '@material-ui/core'
import {BASE_URL} from '../../const'
import React from 'react'
import {makeStyles} from '@material-ui/core/styles'


const useStyles = makeStyles((theme) => ({
    title: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
    },
    hide: {
        display: 'none',
    },
    titleText: {
        fontWeight: 'bold',
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    appBar: {
        backgroundColor: "#00549F",
    },
    logo: {
        maxHeight: 64,
        margin: "1rem",
        marginRight: '10px',
        justifySelf: 'left'
    },
}))

function Header() {
    const classes = useStyles();

    return (
        <AppBar position="static" className={classes.appBar}>
            <Grid container>
                <Grid container xs={3} sm={4} md={4}>
                    <img src={`${BASE_URL}/logo.png`} alt="WZL Logo" className={classes.logo}/>
                </Grid>
                <Grid container xs={6} sm={4} md={4} className={classes.title}>
                    <Typography variant="h3" className={classes.titleText}>Dr. Benjamin Montavon</Typography>
                </Grid>
                <Grid container xs={3} sm={4} md={4} style={{justifyContent: 'right'}}>
                    <img src={`${BASE_URL}/logo.png`} alt="WZL Logo" className={classes.logo}/>
                </Grid>
            </Grid>
        </AppBar>
    );
}

export default Header;
