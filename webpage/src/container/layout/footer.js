import './Layout.css';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from "@material-ui/core/styles";

const useStyles =  makeStyles((theme) => ({
    appBar: {
        marginTop: "2rem",
        backgroundColor: "#cfd1d2",
    },
    footerBar: {
        display: "flex",
        color: "#000000",
        flexDirection: "column",
        justifyContent: "center",
    }
}))

function Footer() {
    const classes = useStyles();

    return (
        <AppBar position="static" className={classes.appBar}>
            <Toolbar className={classes.footerBar}>
                <Typography variant="p">
                    14.09.2021 | mnt_mb
                </Typography>
            </Toolbar>
        </AppBar>
    );
}

export default Footer;
