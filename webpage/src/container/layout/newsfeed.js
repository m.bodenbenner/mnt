import './Layout.css'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import {makeStyles} from '@material-ui/core/styles'
import {useSelector} from 'react-redux'
import {Slide} from '@material-ui/core'
import Carousel from 'react-material-ui-carousel'

const useStyles = makeStyles((theme) => ({
  newsBar: {
    marginTop: '2rem',
    backgroundColor: theme.palette.error.light
  },
  footerBar: {
    display: 'flex',
    color: '#000000',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  paper: {
    zIndex: 1,
    position: 'relative',
    margin: theme.spacing(1)
  },
  carousel: {

  }
}))

const selectedMessages = state => state.messages

function NewsFeed() {
  const classes = useStyles()
  const messages = useSelector(selectedMessages)


  let news = []
  let jointMessages = []

  for (let topic in messages) {
    jointMessages = jointMessages.concat(messages[topic])

  }

  jointMessages.sort( (a,b) =>  a.timestamp < b.timestamp ? -1 : 1)

  jointMessages.forEach((message, idx) => {
    if (message.topic.includes('Temperature')) {
      news.push(
        <Typography key={message.topic + idx.toString()} variant="h6">
          +++ {Date(message.timestamp).toLocaleString()} - {message.topic}: {message.value} °C +++
        </Typography>
      )
    }
  })

  return (
    <AppBar position="static" className={classes.newsBar}>
      <Toolbar className={classes.footerBar}>
        <Typography variant="h6">
          <Carousel interval={5000} animation="slide" indicators={false} className={classes.carousel} reverseEdgeAnimationDirection={false} timeout={1000}>
              {news}
          </Carousel>
        </Typography>
      </Toolbar>
    </AppBar>
  )
}

export default NewsFeed
