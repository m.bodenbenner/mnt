import { createTheme } from '@material-ui/core/styles';

export const RWTHTheme = createTheme({
    palette: {
        primary: {
            main: '#00549F',
        },
        secondary: {
            main: '#E30066',
        },
        error: {
            main: '#A11035',
        },
        warning: {
            main: '#F6A800',
        },
        info: {
            main: '#006165',
        },
        success: {
            main: '#57AB27',
        },
        contrastThreshold: 3,
        tonalOffset: 0.2,
    },
});