import Layout from './container/layout/layout'
import {Provider} from 'react-redux'
import rootReducer from './reducer'
import {applyMiddleware, compose, createStore} from 'redux'
import {RWTHTheme} from './rwthTheme'
import {ThemeProvider} from '@material-ui/styles'
import thunk from 'redux-thunk'
import mqttMiddleware from './middlewares/mqtt'

const store = createStore(
    rootReducer(),
    compose(
      applyMiddleware(
        mqttMiddleware,
        thunk,
      )
    )
)

function App() {

  return (
    <Provider store={store}>
      <ThemeProvider theme={RWTHTheme}>
        <Layout/>
      </ThemeProvider>
    </Provider>
  )
}

export default App
