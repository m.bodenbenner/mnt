export function mqttConnect(address, username, password, vhost = null) {
    console.log('mqttConnect')
    return {
        type: 'MQTT:CONNECT',
        address: address,
        username: username,
        password: password,
        vhost: vhost
    }
}

export function mqttSubscribe(topic) {
    return {
        type: 'MQTT:SUBSCRIBE',
        topic: topic,
    }
}

export function mqttUnsubscribe(topic) {
    return {
        type: 'MQTT:UNSUBSCRIBE',
        topic: topic,
    }
}
