const initialState = {}
export const messages = (state = initialState, action) => {
  let tempState = JSON.parse(JSON.stringify(state))

  switch (action.type) {
    case 'MQTT:MESSAGE':
      if (!tempState.hasOwnProperty(action.topic)){
        tempState[action.topic] = []
      }
      if (tempState[action.topic].length > 9) {
        tempState[action.topic].splice(0,1)
      }
      if (action.message.hasOwnProperty('timestamp')) {
        action.message.timestamp = Date.parse(action.message.timestamp)
      }
      action.message.topic = action.topic
      tempState[action.topic].push(action.message)
      break
  }
  console.log(tempState)
  return tempState
}
