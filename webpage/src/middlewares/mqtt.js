var mqtt = require('mqtt')

const mqttMiddleware = store => {
  let client = null
  const subscriptions = []

  const on_connect_success = () => {
    console.log('Connected')
    subscriptions.forEach(
      topic => {
        try {
          client.subscribe(topic)
        } catch (error) {
          store.dispatch({type: 'EXCEPTION:MQTT', message: error.message})
          console.error(error)
        }
      }
    )
  }

  const on_message = (topic, message, packet) => {
    store.dispatch({
      type: 'MQTT:MESSAGE',
      message: JSON.parse(message.toString()),
      topic: topic,
    })
    console.log('MQTT Topic ' + topic)
    console.log('MQTT Message ' + message)
  }
  console.log('MQTTMIDDLEWARE')

  return next => action => {
    try {
      if (action.type === 'MQTT:CONNECT') {
        console.log('connecting')
        if (client === null) {
          const username = action.vhost !== null ? `${action.vhost}:${action.username}` : action.username
          client = mqtt.connect(`mqtt://${action.address}:15675/ws`, {username: username, password: action.password})
          client.on('connect', on_connect_success)
          client.on('message', on_message)
        }
      } else if (action.type === 'MQTT:DISCONNECT') {
        if (client !== null) {
          client.end()
        } else {
          throw new Error('MQTT Client has never been created')
        }
      } else if (action.type === 'MQTT:SUBSCRIBE') {
        if (client !== null) {
          if (!subscriptions.includes(action.topic)) {
            subscriptions.push(action.topic)
            client.subscribe(action.topic)
          }
        }
      } else if (action.type === 'MQTT:UNSUBSCRIBE') {
        if (client !== null) {
          if (subscriptions.includes(action.topic)) {
            const index = subscriptions.indexOf(action.topic)
            subscriptions.splice(index, 1)
            client.unsubscribe(action.id, action.topic, action.qos)
          }
        }
      }
    } catch (error) {
      store.dispatch({type: 'EXCEPTION:MQTT', message: error.message})
      console.log(error.message)
    }
    console.log(subscriptions)
    next(action)
  }
}

export default mqttMiddleware
